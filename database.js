let db_name = "db/tbt.db";

const sqlite3 = require('sqlite3').verbose();
 
//https://github.com/mapbox/node-sqlite3/issues/674
var db = new sqlite3.Database(db_name, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, function (err) {
    if (err) console.log(err.message);
});

db.serialize(function() {
    db.run(`CREATE TABLE IF NOT EXISTS QNA(
    QNA_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    QNA_QUESTION TEXT NOT NULL,
    QNA_ANSWER TEXT NOT NULL,
    QNA_CORRECT_ANSWER TEXT,
    QNA_DIR_IMAGE TEXT,
    QNA_DIR_IMAGE_BASE64 TEXT,
    QNA_DIR_OTHER TEXT
    );`);
    
    db.run(`CREATE TABLE IF NOT EXISTS SETTING(
    SETTING_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    SETTING_ID2 TEXT,
    SETTING_NAME TEXT NOT NULL,
    SETTING_VALUE TEXT NOT NULL
    );`);

    db.run(`CREATE TABLE IF NOT EXISTS TEAM(
    TEAM_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    TEAM_NAME TEXT,
    TEAM_SCORE TEXT,
    TEAM_DIR_IMAGE TEXT,
    TEAM_DIR_IMAGE_BASE64 TEXT
    );`);
    
    var stmt = db.prepare("INSERT INTO TEAM (TEAM_ID, TEAM_NAME, TEAM_SCORE) VALUES (?, ?, ?);")
    stmt.run("1", "", "");
    stmt.run("2", "", "");
    stmt.run("3", "", "");
    stmt.run("4", "", "");
    stmt.run("5", "", "");
    stmt.run("6", "", "");
    stmt.run("7", "", "");
    stmt.run("8", "", "");
    stmt.run("9", "", "");
    stmt.run("10", "", "");
    stmt.finalize()
    
    /*
    //Insert data
    var stmt = db.prepare("INSERT INTO QNA (QNA_QUESTION, QNA_ANSWER) VALUES (?, ?);")
    stmt.run("TestQuestion", "TestAnswer");
    stmt.finalize()
    
    //Select data
    db.each("SELECT * FROM QNA", function(err, row){
        console.log(row.QNA_ID + " - " + row.QNA_QUESTION + " - " + row.QNA_ANSWER);  
    })
    */
    
    /*db.each("SELECT * FROM QNA", function(err, row){
        console.log(row.QNA_ID + " - " + row.QNA_QUESTION + " - " + row.QNA_ANSWER);  
    })*/
});
 
db.close((err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Close the database connection.');
});

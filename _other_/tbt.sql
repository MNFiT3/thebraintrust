CREATE TABLE IF NOT EXISTS QNA(
    QNA_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    QNA_QUESTION TEXT NOT NULL,
    QNA_ANSWER TEXT NOT NULL,
    QNA_CORRECT_ANSWER TEXT,
    QNA_DIR_IMAGE TEXT,
    QNA_DIR_IMAGE_BASE64 TEXT,
    QNA_DIR_OTHER TEXT
);

CREATE TABLE IF NOT EXISTS SETTING(
    SETTING_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    SETTING_ID2 TEXT,
    SETTING_NAME TEXT NOT NULL,
    SETTING_VALUE TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS TEAM(
    TEAM_ID INTEGER NOT NULL PRIMARY KEY,
    TEAM_NAME TEXT,
    TEAM_SCORE TEXT,
    TEAM_DIR_IMAGE TEXT,
    TEAM_DIR_IMAGE_BASE64 TEXT
);

INSERT INTO TEAM (TEAM_ID) VALUES (1);
INSERT INTO TEAM (TEAM_ID) VALUES (2);
INSERT INTO TEAM (TEAM_ID) VALUES (3);
INSERT INTO TEAM (TEAM_ID) VALUES (4);
INSERT INTO TEAM (TEAM_ID) VALUES (5);
INSERT INTO TEAM (TEAM_ID) VALUES (6);
INSERT INTO TEAM (TEAM_ID) VALUES (7);
INSERT INTO TEAM (TEAM_ID) VALUES (8);
INSERT INTO TEAM (TEAM_ID) VALUES (9);
INSERT INTO TEAM (TEAM_ID) VALUES (10);

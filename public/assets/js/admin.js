var socket = io(),
    QNASheet,
    QuestIndex,
    displayTime = document.getElementById('timer'),
    times= 10,
    fiveMinutes,
    minutes, 
    seconds,
    timerInterval;

function init(){
    index0 = 1;
    socket.emit("Server_loadData");
    $('#QuestionNO').append($('<option>', { value : -1 }).text("Choose..."));
}

function hidePage(){
    document.getElementById("menu1").style.display = "none";
    document.getElementById("menu2").style.display = "none";
    document.getElementById("menu3").style.display = "none";
    document.getElementById("menu4").style.display = "none";
    document.getElementById('loading').style.display = "none";
}

function gotoPage(id){
    hidePage();
    if (id == 1){
        document.getElementById("menu1").style.display = "block";
    }else if (id == 2){
        document.getElementById("menu2").style.display = "block";
    }else if (id == 3){
        document.getElementById("menu3").style.display = "block";
    }else if (id == 4){
        document.getElementById('loading').style.display = "block"
        setTimeout(function(){
            hidePage();
            gotoPage(2);
            alert("Saved Succesfully!");
        },3000);
    }else if (id == 5){
        document.getElementById("menu4").style.display = "block";
    }
}
function DisplayScore(flag){
    socket.emit("Server_DisplayScore", flag);
}

function sendQuestion(){
    var questionNo = document.getElementById("QuestionNO").value
    var questionText = document.getElementById("Question").value
    socket.emit("sendQuestion", questionNo, questionText)
}

function UpdateScore(){
    gotoPage(4);
    var scoreData = [];
    for(var i = 0; i < 10; i++){
        var index = (i + 1);
        var tn = document.getElementById("team" + index).value;
        var ts = document.getElementById("team" + index + "_score").value;
        scoreData.push({"TeamID" : index, "TeamName" : tn, "TeamScore" : ts});
    }
    var obj = JSON.stringify(scoreData);
    socket.emit("UpdateTeamScore", obj);
    
    
}

function playSound(eID){
    if(eID == "btnCorrect"){
        var audio = document.getElementById("sound_correct")
        audio.volume=0.05;
        audio.play();
        socket.emit("AnswerResult", true)
    }else if(eID == "btnWrong"){
       var audio =  document.getElementById("sound_wrong")
       audio.volume=0.05;
       audio.play();
        socket.emit("AnswerResult", false)
    }
}

function timerStart(min){
    console.log("timer start...")
    times=min*60;
    timerInterval = setInterval(function () {
            
        
        minutes = parseInt(times / 60, 10)
        seconds = parseInt(times % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        displayTime.innerHTML = minutes + ":" + seconds;
        if (--times < 0) {
                clearInterval(timerInterval);
        }     
    }, 1000);
    socket.emit("timer", "start"+min);    
}
function timerStop(){
    console.log("timer stop...");
    clearInterval(timerInterval);
    socket.emit("timer", "stop");     
}

function timerReset(){
    console.log("timer reset...");
    times=0;
    minutes = parseInt(times / 60, 10)
    seconds = parseInt(times % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    displayTime.innerHTML = minutes + ":" + seconds;
    clearInterval(timerInterval);
    socket.emit("timer", "reset");    
}

function Setting(value){
    hidePage();
    document.getElementById('loading').style.display = "block"
    setTimeout(function(){
        hidePage();
        gotoPage(3);
        alert("Operation Succesfully Executed!");
    },3000);
    if (value == 1){
        socket.emit("Server_ReloadData");
    }else if (value == 2){
        if (confirm("ALL DATA WILL BE DELETED!")){
            socket.emit("Server_ResetDatabase");
        }
    }
    
}

function QuestControl(i){
    if(i == 0 && QuestIndex >= 0 ){
        socket.emit("QuestIndex", QuestIndex);
    }
}

function AdminControl(cmd){
    socket.emit(cmd);
}

function videoControl(index){
    socket.emit("video", index)
}

socket.on('QNASheet', function(data){
    $('#QuestionNO').find('option').remove().end();
    $('#QuestionNO').append($('<option>', { value : -1 }).text("Choose..."));
    QNASheet = JSON.parse(data);
    QNASheet.forEach(function(obj){
        $('#QuestionNO').append($('<option>', { value : obj.QID }).text(obj.QID + ". " + obj.Type));
    })
});

socket.on('whosin', function(data){
    var tnames = JSON.parse(data)
    $( '#whosin' ).empty();
    tnames.forEach(function(name){
        $('#whosin').append('<li><p class="text-dark">'+name.tname+'</p></li>');
    })  
});

socket.on('Admin_TeamScoreData', function(data){
    var obj = JSON.parse(data);
    for(var i = 0; i < 10; i++){
        var index = (i + 1);
        document.getElementById("team" + index).value = obj[i].TEAMNAME;
        document.getElementById("team" + index + "_score").value = obj[i].TEAMSCORE;
    }
})

//listen events
socket.on('whosin', function(data){
    var tnames = JSON.parse(data)
    $( '#whosin' ).empty();
    tnames.forEach(function(name){
        $('#whosin').append('<li><p class="text-dark">'+name.tname+'</p></li>');
    })  
});


$(document).on('change', 'select', function() {
    var index = $(this).val();
    var textarea = document.getElementById('Question')
    var textarea1 = document.getElementById('Answer')
    var QuestionImage = document.getElementById('QuestionImage')
    
    QuestIndex = index - 1;
    
    if (index == -1){
        textarea.value = "";
        textarea.value += "";
        textarea1.value = "";
        QuestionImage.src = "assets/image/dummy_no_image.png";
    }else{
        textarea.value = QNASheet[index - 1].Question;
        textarea.value += "\n" + QNASheet[index - 1].Answer;
        textarea1.value = QNASheet[index - 1].CorrectAnswer;
        if (QNASheet[index - 1].Image == null) {
        QuestionImage.src = "assets/image/dummy_no_image.png";
        }
        else{
        QuestionImage.src = "/upload/" + QNASheet[index - 1].Image;
        }
    }
});

$('#uploadFile').bind('change', function() {
    var filename = this.files[0].name;
    var arr = filename.split(".");
    var valid = false;
    for (var i = 0; i < filename.length; i++){
        if (typeof arr[i] != 'undefined'){
            var str = arr[i].toLowerCase();
            if (str == "png"){
                valid = true;
            }
        }
    }
    if (!valid){
        alert("File Not Suported")
        document.getElementById("uploadFile").value = "";
    }
});
